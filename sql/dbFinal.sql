-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 07, 2022 at 11:18 AM
-- Server version: 10.3.35-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kreatordn_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `delavci`
--

CREATE TABLE `delavci` (
  `id` int(11) NOT NULL,
  `ime` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priimek` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aktivan` tinyint(1) DEFAULT 1,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delavci`
--

INSERT INTO `delavci` (`id`, `ime`, `priimek`, `aktivan`, `created_at`, `updated_at`) VALUES
(21, 'Vlado', 'Solakov', 1, '2022-06-06', '2022-06-06'),
(23, 'Radovan', 'Radic', 1, '2022-06-06', '2022-06-06'),
(24, 'Emilija', 'Mitrovic', 1, '2022-06-06', '2022-06-06');

-- --------------------------------------------------------

--
-- Table structure for table `delovni_nalog`
--

CREATE TABLE `delovni_nalog` (
  `id` int(11) NOT NULL,
  `datum_zacetka` date NOT NULL,
  `sifra` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `naziv` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `objekt` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_projekt` tinyint(1) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delovni_nalog`
--

INSERT INTO `delovni_nalog` (`id`, `datum_zacetka`, `sifra`, `naziv`, `objekt`, `status_projekt`, `created_at`, `updated_at`) VALUES
(37, '2022-06-08', 'JB007', 'Strelovodna Instalacija kod Miroslav doma', 'Miroslav\'s Residence', 0, '2022-06-06', '2022-06-06'),
(38, '2022-06-06', 'GGWP', 'Strelovodna instalacija kod Radovan', 'Radovan\'s Residence', 0, '2022-06-06', '2022-06-06');

-- --------------------------------------------------------

--
-- Table structure for table `dnevne_nalog`
--

CREATE TABLE `dnevne_nalog` (
  `id` int(11) NOT NULL,
  `delovni_nalog_id` int(11) NOT NULL,
  `vozilo_id` int(11) NOT NULL,
  `datum` date NOT NULL,
  `startt` time NOT NULL,
  `zacetek_dela` time DEFAULT NULL,
  `konec_dela` time DEFAULT NULL,
  `prihod` time DEFAULT NULL,
  `odsotnost_soferja` float DEFAULT 0,
  `odsotnost_delavca` float DEFAULT 0,
  `neto_cas` float DEFAULT 0,
  `bruto_cas` float DEFAULT 0,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dnevne_nalog`
--

INSERT INTO `dnevne_nalog` (`id`, `delovni_nalog_id`, `vozilo_id`, `datum`, `startt`, `zacetek_dela`, `konec_dela`, `prihod`, `odsotnost_soferja`, `odsotnost_delavca`, `neto_cas`, `bruto_cas`, `created_at`, `updated_at`) VALUES
(257, 37, 10, '2022-06-06', '12:20:00', '13:00:00', '18:00:00', '19:00:00', 6.66667, 6.66667, 15, 20, '2022-06-06', '2022-06-06'),
(258, 37, 10, '2022-06-06', '06:05:00', '07:00:00', '18:05:00', '19:05:00', 13, 11.0833, 22.1667, 24.0833, '2022-06-06', '2022-06-06'),
(259, 38, 11, '2022-06-06', '06:16:00', '07:25:00', '13:05:00', '14:14:00', 7.96667, 7.96667, 17, 23.9, '2022-06-06', '2022-06-06'),
(260, 37, 10, '2022-06-06', '06:04:00', '07:34:00', '13:05:00', '19:10:00', 13.1, 8, 11.0333, 21.1, '2022-06-06', '2022-06-06');

-- --------------------------------------------------------

--
-- Table structure for table `dnevne_nalog_delavci`
--

CREATE TABLE `dnevne_nalog_delavci` (
  `id` int(11) NOT NULL,
  `dnevne_nalog_id` int(11) NOT NULL,
  `delavci_id` int(11) NOT NULL,
  `je_sofer` tinyint(1) DEFAULT 0,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dnevne_nalog_delavci`
--

INSERT INTO `dnevne_nalog_delavci` (`id`, `dnevne_nalog_id`, `delavci_id`, `je_sofer`, `created_at`, `updated_at`) VALUES
(438, 257, 23, 0, '2022-06-06', '2022-06-06'),
(439, 257, 24, 0, '2022-06-06', '2022-06-06'),
(440, 257, 21, 1, '2022-06-06', '2022-06-06'),
(451, 259, 21, 0, '2022-06-06', '2022-06-06'),
(452, 259, 23, 0, '2022-06-06', '2022-06-06'),
(453, 259, 24, 1, '2022-06-06', '2022-06-06'),
(456, 258, 23, 0, '2022-06-06', '2022-06-06'),
(457, 258, 24, 1, '2022-06-06', '2022-06-06'),
(460, 260, 23, 0, '2022-06-06', '2022-06-06'),
(461, 260, 21, 1, '2022-06-06', '2022-06-06');

-- --------------------------------------------------------

--
-- Table structure for table `vozilo`
--

CREATE TABLE `vozilo` (
  `id` int(11) NOT NULL,
  `naziv` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aktivan` tinyint(1) DEFAULT 1,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vozilo`
--

INSERT INTO `vozilo` (`id`, `naziv`, `aktivan`, `created_at`, `updated_at`) VALUES
(10, 'Zastava 101', NULL, '2022-06-06', '2022-06-06'),
(11, 'BMW x6', NULL, '2022-06-06', '2022-06-06'),
(12, 'PEUGEOT', NULL, '2022-06-06', '2022-06-06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `delavci`
--
ALTER TABLE `delavci`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delovni_nalog`
--
ALTER TABLE `delovni_nalog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dnevne_nalog`
--
ALTER TABLE `dnevne_nalog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delovne_nalog_id` (`delovni_nalog_id`),
  ADD KEY `c_vozilo_id` (`vozilo_id`);

--
-- Indexes for table `dnevne_nalog_delavci`
--
ALTER TABLE `dnevne_nalog_delavci`
  ADD PRIMARY KEY (`id`),
  ADD KEY `c_dnevne_nalog_id` (`dnevne_nalog_id`),
  ADD KEY `c_delavci_id` (`delavci_id`);

--
-- Indexes for table `vozilo`
--
ALTER TABLE `vozilo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `delavci`
--
ALTER TABLE `delavci`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `delovni_nalog`
--
ALTER TABLE `delovni_nalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `dnevne_nalog`
--
ALTER TABLE `dnevne_nalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- AUTO_INCREMENT for table `dnevne_nalog_delavci`
--
ALTER TABLE `dnevne_nalog_delavci`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=462;

--
-- AUTO_INCREMENT for table `vozilo`
--
ALTER TABLE `vozilo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dnevne_nalog`
--
ALTER TABLE `dnevne_nalog`
  ADD CONSTRAINT `c_vozilo_id` FOREIGN KEY (`vozilo_id`) REFERENCES `vozilo` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `delovne_nalog_id` FOREIGN KEY (`delovni_nalog_id`) REFERENCES `delovni_nalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dnevne_nalog_delavci`
--
ALTER TABLE `dnevne_nalog_delavci`
  ADD CONSTRAINT `c_delavci_id` FOREIGN KEY (`delavci_id`) REFERENCES `delavci` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `c_dnevne_nalog_id` FOREIGN KEY (`dnevne_nalog_id`) REFERENCES `dnevne_nalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
