<?php

namespace App\Http\Controllers;

use App\Http\Resources\Delovni_nalog_resource;
use App\Http\Resources\Dnevne_nalog_resource;
use App\Models\Delovni_nalog;
use App\Models\Dnevne_nalog;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Error;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Facades\Http;
use Exception;

class Delovni_nalog_controller extends Controller
{
    function authAPI($jwt)
    {
        try {
            $publicKeys = Http::get('https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com');
            $publicKey = $publicKeys[array_key_first($publicKeys->json())];
            $payload = JWT::decode($jwt, new Key($publicKey, 'RS256'));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
<<<<<<< HEAD
        if ($request->key == "tempKey123!") {
            $delovni_nalog = Delovni_nalog::with('dnevne_nalog')->paginate(100);
            foreach($delovni_nalog as $projekt) {
=======
        if ($this->authAPI($request->key)) {
            $delovni_nalog = Delovni_nalog::with('dnevne_nalog')->paginate(50);
            foreach ($delovni_nalog as $projekt) {
                $neto = Dnevne_nalog::where('delovni_nalog_id', $projekt->id)->sum('neto_cas');
                $bruto = Dnevne_nalog::where('delovni_nalog_id', $projekt->id)->sum('bruto_cas');
                $projekt->neto_cas = $neto;
                $projekt->bruto_cas = $bruto;
>>>>>>> dnevne-naloge
                unset($projekt->dnevne_nalog);
            }
            return Delovni_nalog_resource::collection($delovni_nalog);
        } else
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->authAPI($request->key)) {
            $delovni_nalog = new Delovni_nalog();
            $delovni_nalog->datum_zacetka = $request->datum_zacetka;
            $delovni_nalog->sifra = $request->sifra;
            $delovni_nalog->naziv = $request->naziv;
            $delovni_nalog->objekt = $request->objekt;
            $delovni_nalog->status_projekt = $request->status_projekt;
            if ($delovni_nalog->save()) {
                $response = ['method' => 'post', 'response' => 'success'];
                return $response;
            }
        } else
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
<<<<<<< HEAD
        if ($request->key == "tempKey123!") {
            $delovni_nalog = Delovni_nalog::with('dnevne_nalog')->get()->where('id', $id);
            foreach($delovni_nalog->first()->dnevne_nalog as $item) {
                foreach($item->dnevne_nalog_delavci as $delavec) {
=======
        if ($this->authAPI($request->key)) {
            $delovni_nalog = Delovni_nalog::with('dnevne_nalog')->get()->where('id', $id);
            $neto = Dnevne_nalog::where('delovni_nalog_id', $id)->sum('neto_cas');
            $bruto = Dnevne_nalog::where('delovni_nalog_id', $id)->sum('bruto_cas');
            $delovni_nalog->first()->neto_cas = $neto;
            $delovni_nalog->first()->bruto_cas = $bruto;
            error_log($neto);
            foreach ($delovni_nalog->first()->dnevne_nalog as $item) {
                foreach ($item->dnevne_nalog_delavci as $delavec) {
>>>>>>> dnevne-naloge
                    $delavec->je_sofer = $delavec->pivot->je_sofer;
                    unset($delavec->pivot);
                }
                $vozilo = $item->vozilo->first()->naziv;
                unset($item->vozilo);
                $item->vozilo = $vozilo;
            }

            return Delovni_nalog_resource::collection($delovni_nalog);
        } else
            return ['method' => 'get', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->authAPI($request->key)) {
            $delovni_nalog = Delovni_nalog::findOrFail($id);
            $delovni_nalog->datum_zacetka = $request->datum_zacetka;
            $delovni_nalog->sifra = $request->sifra;
            $delovni_nalog->naziv = $request->naziv;
            $delovni_nalog->objekt = $request->objekt;
            $delovni_nalog->status_projekt = $request->status_projekt;
            if ($delovni_nalog->save()) {
                $response = ['method' => 'put', 'response' => 'success'];
                return $response;
            }
        } else
            return ['method' => 'put', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($this->authAPI($request->key)) {
            $delovni_nalog = Delovni_nalog::findOrFail($id);
            if ($delovni_nalog->delete()) {
                $response = ['method' => 'delete', 'response' => 'success'];
                return $response;
            }
        } else
            return ['method' => 'delete', 'response' => 'fail', 'reason' => 'Token expired'];
    }
}
