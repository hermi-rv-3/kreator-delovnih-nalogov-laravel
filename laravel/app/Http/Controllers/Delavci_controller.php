<?php

namespace App\Http\Controllers;

use App\Http\Resources\Delavci_resource;
use App\Models\Delavci;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Error;

use function PHPUnit\Framework\isNull;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Facades\Http;
use Exception;

class Delavci_controller extends Controller
{
    function authAPI($jwt)
    {
        try {
            $publicKeys = Http::get('https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com');
            $publicKey = $publicKeys[array_key_first($publicKeys->json())];
            $payload = JWT::decode($jwt, new Key($publicKey, 'RS256'));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
<<<<<<< HEAD
        if ($request->key == "tempKey123!") {
=======
        if ($this->authAPI($request->key)) {
>>>>>>> dnevne-naloge
            if($request->showAll == 0)
                $delavci = Delavci::where('aktivan', 1)->paginate(50);
            else
                $delavci = Delavci::paginate(50);
            return Delavci_resource::collection($delavci);
        } else
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD
        if ($request->key == "tempKey123!") {
=======
        if ($this->authAPI($request->key)) {
>>>>>>> dnevne-naloge
            $delavci = new Delavci();
            $delavci->ime = $request->ime;
            $delavci->priimek = $request->priimek;
            $delavci->aktivan = $request->aktivan;
            if ($delavci->save()) {
                $response = ['method' => 'post', 'response' => 'success'];
                return $response;
            }
        } else
<<<<<<< HEAD
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'key missing or incorrect'];
=======
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'Token expired'];
>>>>>>> dnevne-naloge
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($this->authAPI($request->key)) {
            $delavci = Delavci::with('dnevne_nalog')->get()->where('id', $id);
            $totalCas = 0;
            foreach ($delavci->first()->dnevne_nalog as $dnevnaNaloga) {
                if($dnevnaNaloga->je_sofer == 1) {
                    $totalCas += Carbon::parse($dnevnaNaloga->prihod)->diffInSeconds(Carbon::parse($dnevnaNaloga->startt)) / 3600;
                } else {
                    $totalCas += Carbon::parse($dnevnaNaloga->konec_dela)->diffInSeconds(Carbon::parse($dnevnaNaloga->zacetek_dela)) / 3600;
                }
            }
            unset($delavci->first()->dnevne_nalog);
            $delavci->first()->monterjev_cas = $totalCas;
            return new Delavci_resource($delavci);
        } else
            return ['method' => 'get', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
<<<<<<< HEAD
        if ($request->key == "tempKey123!") {
=======
        if ($this->authAPI($request->key)) {
>>>>>>> dnevne-naloge
            $delavci = Delavci::findOrFail($id);
            $delavci->ime = $request->ime;
            $delavci->priimek = $request->priimek;
            $delavci->aktivan = $request->aktivan;
            if ($delavci->save()) {
                $response = ['method' => 'put', 'response' => 'success'];
                return $response;
            }
        } else
<<<<<<< HEAD
            return ['method' => 'get', 'response' => 'fail', 'reason' => 'key missing or incorrect'];
=======
            return ['method' => 'get', 'response' => 'fail', 'reason' => 'Token expired'];
>>>>>>> dnevne-naloge
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
<<<<<<< HEAD
        if ($request->key == "tempKey123!") {
=======
        if ($this->authAPI($request->key)) {
>>>>>>> dnevne-naloge
            $delavci = Delavci::findOrFail($id);
            if ($delavci->delete()) {
                $response = ['method' => 'delete', 'response' => 'success'];
                return $response;
<<<<<<< HEAD
            }
        } else
            return ['method' => 'delete', 'response' => 'fail', 'reason' => 'key missing or incorrect'];
    }

    public function obseg(Request $request, $id)
    {
        if ($request->key == "tempKey123!") {
            $i = 0;
            $od = $request->od;
            $do = $request->do;
            $od = date('Y-m-d', strtotime($od));
            $do = date('Y-m-d', strtotime($do));
            $delavci = Delavci::with('dnevne_nalog')->where('id', $id)->paginate(50);
            $totalCas = 0;
            foreach ($delavci[0]->dnevne_nalog as $dnevnaNaloga) {
                $dnevnaNaloga->je_sofer = $dnevnaNaloga->pivot->je_sofer;
                unset($dnevnaNaloga->pivot);
                $vozilo = $dnevnaNaloga->vozilo->first()->naziv;
                unset($dnevnaNaloga->vozilo);
                $dnevnaNaloga->vozilo = $vozilo;
                $datum = date('Y-m-d', strtotime($dnevnaNaloga->datum));
                if (($datum < $od) || ($datum > $do)) {
                    unset($delavci[0]->dnevne_nalog[$i]);
                } else {
                    if($dnevnaNaloga->je_sofer) {
                        $totalCas += Carbon::parse($dnevnaNaloga->prihod)->diffInSeconds(Carbon::parse($dnevnaNaloga->startt));
                    } else {
                        $totalCas += Carbon::parse($dnevnaNaloga->konec_dela)->diffInSeconds(Carbon::parse($dnevnaNaloga->zacetek_dela));
                    }
                }
                $i++;
            }
            $delavci[0]->monterjev_cas = gmdate('H:i', $totalCas);
            return Delavci_resource::collection($delavci);
=======
            }
>>>>>>> dnevne-naloge
        } else
            return ['method' => 'delete', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    public function showDnevneNalog(Request $request, $id) 
    {
        if ($request->key == "tempKey123!") {
            $delavci = Delavci::with('dnevne_nalog')->where('id', $id)->paginate(5);
            $totalCas = 0;
            foreach ($delavci[0]->dnevne_nalog as $dnevnaNaloga) {
                $dnevnaNaloga->je_sofer = $dnevnaNaloga->pivot->je_sofer;
                unset($dnevnaNaloga->pivot);
                $vozilo = $dnevnaNaloga->vozilo->first()->naziv;
                unset($dnevnaNaloga->vozilo);
                $dnevnaNaloga->vozilo = $vozilo;
            }
            unset($delavci[0]->id);
            unset($delavci[0]->ime);
            unset($delavci[0]->priimek);
            unset($delavci[0]->aktivan);
            unset($delavci[0]->created_at);
            unset($delavci[0]->updated_at);
            return new Delavci_resource($delavci);
        } else
            return ['method' => 'get', 'response' => 'fail', 'reason' => 'key missing or incorrect'];
    }
}
