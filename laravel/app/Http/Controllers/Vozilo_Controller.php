<?php

namespace App\Http\Controllers;

use App\Http\Resources\Vozilo_resource;
use App\Models\Vozilo;
use Illuminate\Http\Request;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Facades\Http;
use Exception;

class Vozilo_Controller extends Controller
{
    function authAPI($jwt)
    {
        try {
            $publicKeys = Http::get('https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com');
            $publicKey = $publicKeys[array_key_first($publicKeys->json())];
            $payload = JWT::decode($jwt, new Key($publicKey, 'RS256'));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
<<<<<<< HEAD
        if ($request->key == "tempKey123!") {
            $vozilo = Vozilo::paginate(1000000);
=======
        if ($this->authAPI($request->key)) {
            if ($request->showAll == 0)
                $vozilo = Vozilo::where('aktivan', 1)->paginate(1000000);
            else
                $vozilo = Vozilo::paginate(1000000);
>>>>>>> dnevne-naloge
            return Vozilo_resource::collection($vozilo);
        } else
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->authAPI($request->key)) {
            $vozilo = new Vozilo();
            $vozilo->naziv = $request->naziv;
            $vozilo->aktivan = $request->aktivan;
            if ($vozilo->save()) {
                $response = ['method' => 'post', 'response' => 'success'];
                return $response;
            }
        } else
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($this->authAPI($request->key)) {
            $vozilo = Vozilo::findOrFail($id);
            return new Vozilo_resource($vozilo);
        } else
            return ['method' => 'get', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->authAPI($request->key)) {
            $vozilo = Vozilo::findOrFail($id);
            $vozilo->naziv = $request->naziv;
            $vozilo->aktivan = $request->aktivan;
            if ($vozilo->save()) {
                $response = ['method' => 'put', 'response' => 'success'];
                return $response;
            }
        } else
            return ['method' => 'get', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($this->authAPI($request->key)) {
            $vozilo = Vozilo::findOrFail($id);
            if ($vozilo->delete()) {
                $response = ['method' => 'delete', 'response' => 'success'];
                return $response;
            }
        } else
            return ['method' => 'delete', 'response' => 'fail', 'reason' => 'Token expired'];
    }
}
