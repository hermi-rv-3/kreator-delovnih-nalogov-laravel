<?php

namespace App\Http\Controllers;

use App\Exports\DnevneExport as ExportsDnevneExport;
use App\Http\Resources\Dnevne_nalog_resource;
use App\Models\Dnevne_nalog;
use App\Models\Delovni_nalog;
use App\Http\Resources\Dnevne_nalog_Delavci_resource;
use App\Models\Dnevne_nalog_delavci;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
<<<<<<< HEAD
=======
use Maatwebsite\Excel\Facades\Excel;
use App\Providers\DnevneExport;

use function PHPUnit\Framework\isEmpty;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Facades\Http;
use Exception;
>>>>>>> dnevne-naloge

class Dnevne_nalog_controller extends Controller
{
    function authAPI($jwt)
    {
        try {
            $publicKeys = Http::get('https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com');
            $publicKey = $publicKeys[array_key_first($publicKeys->json())];
            $payload = JWT::decode($jwt, new Key($publicKey, 'RS256'));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->authAPI($request->key)) {
            $dnevne_nalog = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->paginate(50);
<<<<<<< HEAD
            $totalBruto = 0;
            $totalNeto = 0;
            $counter = 0;
            foreach ($dnevne_nalog as $dnevnaNaloga) {
                $counter++;
                $totalBruto += $dnevnaNaloga->bruto_cas;
                $totalNeto += $dnevnaNaloga->neto_cas;
=======
            $totalBruto = Dnevne_nalog::sum('bruto_cas');
            $totalNeto = Dnevne_nalog::sum('neto_cas');
            foreach ($dnevne_nalog as $dnevnaNaloga) {
>>>>>>> dnevne-naloge
                $vozilo = $dnevnaNaloga->vozilo->first()->naziv;
                unset($dnevnaNaloga->vozilo);
                $dnevnaNaloga->vozilo = $vozilo;
                foreach ($dnevnaNaloga->dnevne_nalog_delavci as $delavec) {
                    $delavec->je_sofer = $delavec->pivot->je_sofer;
                    unset($delavec->pivot);
                }
                $dnevnaNaloga->delovni_nalog->first()->neto_cas = Dnevne_nalog::where('delovni_nalog_id', $dnevnaNaloga->delovni_nalog->first()->id)->sum('neto_cas');
                $dnevnaNaloga->delovni_nalog->first()->bruto_cas = Dnevne_nalog::where('delovni_nalog_id', $dnevnaNaloga->delovni_nalog->first()->id)->sum('bruto_cas');
            }
            $dnevne_nalog[0]->totalNeto = $totalNeto;
            $dnevne_nalog[0]->totalBruto = $totalBruto;
            return Dnevne_nalog_resource::collection($dnevne_nalog);
        } else
            return ['method' => 'get', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->authAPI($request->key)) {
            $dnevne_nalog = new Dnevne_nalog();
            $dnevne_nalog->delovni_nalog_id = $request->delovni_nalog_id;
            $dnevne_nalog->vozilo_id = $request->vozilo_id;
            $dnevne_nalog->datum = $request->datum;
            $dnevne_nalog->startt = $request->startt;
            $dnevne_nalog->zacetek_dela = $request->zacetek_dela;
<<<<<<< HEAD
            $dnevne_nalog->konec_dela = $request->konec_dela;
            $dnevne_nalog->prihod = $request->prihod;
            $dnevne_nalog->odsotnost_soferja = $request->odsotnost_soferja;
            $dnevne_nalog->odsotnost_delavca = $request->odsotnost_delavca;
            $dnevne_nalog->neto_cas = $request->neto_cas;
            $dnevne_nalog->bruto_cas = $request->bruto_cas;
            $delavci = $request->dnevne_nalog_delavci;
            $neto = 0;
            $bruto = 0;
            if (isset($dnevne_nalog->startt) && isset($dnevne_nalog->zacetek_dela) && isset($dnevne_nalog->konec_dela) && isset($dnevne_nalog->prihod)) {
                $bruto = Carbon::parse($dnevne_nalog->prihod)->diffInSeconds(Carbon::parse($dnevne_nalog->startt)) / 3600;
                $neto = Carbon::parse($dnevne_nalog->konec_dela)->diffInSeconds(Carbon::parse($dnevne_nalog->zacetek_dela)) / 3600;
                $dnevne_nalog->neto_cas = $neto * count($delavci);
                if ($neto < 8 && $bruto >= 8) $neto = 8;
                elseif ($bruto < 8) $neto = $bruto;
                $dnevne_nalog->bruto_cas = $bruto + ($neto * (count($delavci) - 1));
                $dnevne_nalog->odsotnost_soferja = $bruto;
                $dnevne_nalog->odsotnost_delavca = $neto;
            }
            if ($dnevne_nalog->save()) {
                if (isset($dnevne_nalog->startt) && isset($dnevne_nalog->zacetek_dela) && isset($dnevne_nalog->konec_dela) && isset($dnevne_nalog->prihod)) {
                    $neto_cas =  Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->value('neto_cas');
                    $bruto_cas = Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->value('bruto_cas');
                    $neto_cas += $dnevne_nalog->neto_cas;
                    $bruto_cas += $dnevne_nalog->bruto_cas;
                    Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->update(['neto_cas' => $neto_cas, 'bruto_cas' => $bruto_cas]);
                }
=======
            $delavci = $request->dnevne_nalog_delavci;
            if ($dnevne_nalog->save()) {
>>>>>>> dnevne-naloge
                $idDnevnaNalog = $dnevne_nalog->id;
                foreach ($delavci as $delavec) {
                    $dnevne_nalog_delavci = new Dnevne_nalog_delavci();
                    $dnevne_nalog_delavci->dnevne_nalog_id = $idDnevnaNalog;
                    $dnevne_nalog_delavci->delavci_id = $delavec['id'];
                    if (isset($delavec['je_sofer'])) $dnevne_nalog_delavci->je_sofer = $delavec['je_sofer'];
                    $dnevne_nalog_delavci->save();
                }
                $response = ['method' => 'post', 'response' => 'success'];
                return $response;
            }
        } else
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($this->authAPI($request->key)) {
            $dnevne_nalog = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->get()->where('id', $id);
            $vozilo = $dnevne_nalog->first()->vozilo->first()->naziv;
            unset($dnevne_nalog->first()->vozilo);
            $dnevne_nalog->first()->vozilo = $vozilo;
            foreach ($dnevne_nalog->first()->dnevne_nalog_delavci as $delavec) {
                $delavec->je_sofer = $delavec->pivot->je_sofer;
                unset($delavec->pivot);
            }
            return Dnevne_nalog_resource::collection($dnevne_nalog);
        } else
            return ['method' => 'get', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->authAPI($request->key)) {
            $dnevne_nalog = Dnevne_nalog::findOrFail($id);
            $dnevne_nalog->datum = $request->datum;
            $dnevne_nalog->startt = $request->startt;
            $dnevne_nalog->zacetek_dela = $request->zacetek_dela;
            $dnevne_nalog->konec_dela = $request->konec_dela;
            $dnevne_nalog->prihod = $request->prihod;
            $dnevne_nalog->odsotnost_soferja = $request->odsotnost_soferja;
            $dnevne_nalog->odsotnost_delavca = $request->odsotnost_delavca;
            $dnevne_nalog->neto_cas = $request->neto_cas;
            $dnevne_nalog->bruto_cas = $request->bruto_cas;
            $odsotnostSoferja = $dnevne_nalog->odsotnost_soferja;
<<<<<<< HEAD
            $odnostnostDelavca = $dnevne_nalog->odsotnost_delavca;
=======
            $odsotnostDelavca = $dnevne_nalog->odsotnost_delavca;
>>>>>>> dnevne-naloge
            $brutoCas = $dnevne_nalog->bruto_cas;
            $netoCas = $dnevne_nalog->neto_cas;
            $delavci = $request->dnevne_nalog_delavci;
            $neto = 0;
            $bruto = 0;
<<<<<<< HEAD
            $old_neto = 0;
            $old_bruto = 0;
=======
>>>>>>> dnevne-naloge
            if (isset($dnevne_nalog->startt) && isset($dnevne_nalog->zacetek_dela) && isset($dnevne_nalog->konec_dela) && isset($dnevne_nalog->prihod)) {
                $bruto = Carbon::parse($dnevne_nalog->prihod)->diffInSeconds(Carbon::parse($dnevne_nalog->startt)) / 3600;
                $neto = Carbon::parse($dnevne_nalog->konec_dela)->diffInSeconds(Carbon::parse($dnevne_nalog->zacetek_dela)) / 3600;
                $dnevne_nalog->neto_cas = $neto * count($delavci);
                if ($neto < 8 && $bruto >= 8) $neto = 8;
                elseif ($bruto < 8) $neto = $bruto;
                $dnevne_nalog->bruto_cas = $bruto + ($neto * (count($delavci) - 1));
<<<<<<< HEAD
                $old_neto = Dnevne_nalog::where('id', $id)->value('neto_cas');
                $old_bruto = Dnevne_nalog::where('id', $id)->value('bruto_cas');
                $dnevne_nalog->odsotnost_soferja = $bruto;
                $dnevne_nalog->odsotnost_delavca = $neto;
                if(isset ($odsotnostSoferja)) $dnevne_nalog->odsotnost_soferja = $odsotnostSoferja;
                if(isset ($odnostnostDelavca)) $dnevne_nalog->odsotnost_delavca = $odnostnostDelavca;
                if(isset ($netoCas)) $dnevne_nalog->neto_cas = $netoCas;
                if(isset ($brutoCas)) $dnevne_nalog->odsotnost_soferja = $brutoCas;
            }
            if ($dnevne_nalog->save()) {
                if (isset($dnevne_nalog->startt) && isset($dnevne_nalog->zacetek_dela) && isset($dnevne_nalog->konec_dela) && isset($dnevne_nalog->prihod)) {
                    $neto_cas =  Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->value('neto_cas');
                    $bruto_cas = Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->value('bruto_cas');
                    $neto_cas += $dnevne_nalog->neto_cas - $old_neto;
                    $bruto_cas += $dnevne_nalog->bruto_cas - $old_bruto;
                    Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->update(['neto_cas' => $neto_cas, 'bruto_cas' => $bruto_cas]);
                    if(isset ($netoCas)) Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->update(['neto_cas' => $netoCas]);
                    if(isset ($brutoCas)) Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->update(['bruto_cas' => $brutoCas]);
                }
=======
                $dnevne_nalog->odsotnost_soferja = $bruto;
                $dnevne_nalog->odsotnost_delavca = $neto;
            }
            if (isset($odsotnostSoferja) && $odsotnostSoferja != 0) $dnevne_nalog->odsotnost_soferja = $odsotnostSoferja;
            if (isset($odsotnostDelavca) && $odsotnostDelavca != 0) $dnevne_nalog->odsotnost_delavca = $odsotnostDelavca;
            if (isset($netoCas) && $netoCas != 0) $dnevne_nalog->neto_cas = $netoCas;
            if (isset($brutoCas) && $brutoCas != 0) $dnevne_nalog->bruto_cas = $brutoCas;
            if ($dnevne_nalog->save()) {
>>>>>>> dnevne-naloge
                $idDnevnaNalog = $dnevne_nalog->id;
                $dnevne_nalog_delavci = Dnevne_nalog_delavci::where('dnevne_nalog_id', $idDnevnaNalog);
                $dnevne_nalog_delavci->delete();
                foreach ($delavci as $delavec) {
                    $dnevne_nalog_delavci = new Dnevne_nalog_delavci();
                    $dnevne_nalog_delavci->dnevne_nalog_id = $idDnevnaNalog;
                    $dnevne_nalog_delavci->delavci_id = $delavec['id'];
                    $dnevne_nalog_delavci->je_sofer = $delavec['je_sofer'];
                    $dnevne_nalog_delavci->save();
                }
                $response = ['method' => 'post', 'response' => 'success'];
                return $response;
            }
        } else
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($this->authAPI($request->key)) {
            $dnevne_nalog = Dnevne_nalog::findOrFail($id);
            $neto = $dnevne_nalog->neto_cas;
            $bruto = $dnevne_nalog->bruto_cas;
            if ($dnevne_nalog->delete()) {
                $neto_cas =  Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->value('neto_cas') - $neto;
                $bruto_cas = Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->value('bruto_cas') - $bruto;
                Delovni_nalog::where('id', $dnevne_nalog->delovni_nalog_id)->update(['neto_cas' => $neto_cas, 'bruto_cas' => $bruto_cas]);
                $response = ['method' => 'delete', 'response' => 'success'];
                return $response;
            }
        } else
            return ['method' => 'delete', 'response' => 'fail', 'reason' => 'Token expired'];
    }

<<<<<<< HEAD

    public function obseg(Request $request)
    {
        if ($request->key == "tempKey123!") {
            $od = $request->od;
            $do = $request->do;
            $dnevne_nalog = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$od, $do])->paginate(50);
            foreach ($dnevne_nalog as $dnevnaNaloga) {
                $vozilo = $dnevnaNaloga->vozilo->first()->naziv;
                unset($dnevnaNaloga->vozilo);
                $dnevnaNaloga->vozilo = $vozilo;
                foreach ($dnevnaNaloga->dnevne_nalog_delavci as $delavec) {
                    $delavec->je_sofer = $delavec->pivot->je_sofer;
                    unset($delavec->pivot);
=======
    public function search(Request $request)
    {
        if ($this->authAPI($request->key)) {
            $today = date("Y-m-d");
            $old = date("1-01-01");
            $totalNeto = 0;
            $totalBruto = 0;
            $total = 0;
            if (!is_null($request->od) && is_null($request->do)) {
                $dnevne_nalog = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$request->od, $today])->paginate(50);
                $dnevne = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$request->od, $today])->get();
            } elseif (!is_null($request->od) && !is_null($request->do)) {
                $dnevne_nalog = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$request->od, $request->do])->paginate(50);
                $dnevne = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$request->od, $request->do])->get();
            } elseif (is_null($request->od) && !is_null($request->do)) {
                $dnevne_nalog = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$old, $request->do])->paginate(50);
                $dnevne = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$old, $request->do])->get();
            } else {
                $dnevne_nalog = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->paginate(50);
                $dnevne = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->get();
            }
            foreach ($dnevne_nalog as $key => $dnevnaNaloga) {
                $vozilo = $dnevnaNaloga->vozilo->first()->naziv;
                unset($dnevnaNaloga->vozilo);
                $dnevnaNaloga->vozilo = $vozilo;
                if (!is_null($request->vozilo) && strcasecmp($dnevnaNaloga->vozilo, $request->vozilo) != 0) {
                    unset($dnevne_nalog[$key]);
                }
                if (!is_null($request->objekt) && strcasecmp($dnevnaNaloga->delovni_nalog->first()->objekt, $request->objekt) != 0) {
                    unset($dnevne_nalog[$key]);
                }
                if (!is_null($request->sifra) && strcasecmp($dnevnaNaloga->delovni_nalog->first()->sifra, $request->sifra) != 0) {
                    unset($dnevne_nalog[$key]);
                }
                if (!is_null($request->delavec)) {
                    $flag = 0;
                    foreach ($dnevnaNaloga->dnevne_nalog_delavci as $delavec) {
                        $delavec->je_sofer = $delavec->pivot->je_sofer;
                        unset($delavec->pivot);
                        if (strcasecmp($delavec->ime, $request->delavec) == 0) $flag = 1;
                    }
                    if ($flag == 0) {
                        unset($dnevne_nalog[$key]);
                    }
>>>>>>> dnevne-naloge
                }
            }
            foreach ($dnevne as $key => $dnevnaNaloga) {
                $vozilo = $dnevnaNaloga->vozilo->first()->naziv;
                unset($dnevnaNaloga->vozilo);
                $dnevnaNaloga->vozilo = $vozilo;
                if (!is_null($request->vozilo) && strcasecmp($dnevnaNaloga->vozilo, $request->vozilo) != 0) {
                    unset($dnevne[$key]);
                }
                if (!is_null($request->objekt) && strcasecmp($dnevnaNaloga->delovni_nalog->first()->objekt, $request->objekt) != 0) {
                    unset($dnevne[$key]);
                }
                if (!is_null($request->sifra) && strcasecmp($dnevnaNaloga->delovni_nalog->first()->sifra, $request->sifra) != 0) {
                    unset($dnevne[$key]);
                }
                if (!is_null($request->delavec)) {
                    $flag = 0;
                    foreach ($dnevnaNaloga->dnevne_nalog_delavci as $delavec) {
                        $delavec->je_sofer = $delavec->pivot->je_sofer;
                        unset($delavec->pivot);
                        if (strcasecmp($delavec->ime, $request->delavec) == 0) $flag = 1;
                    }
                    if ($flag == 0) {
                        unset($dnevne[$key]);
                    }
                }
                if (isset($dnevne[$key])) {
                    $totalNeto += $dnevnaNaloga->neto_cas;
                    $totalBruto += $dnevnaNaloga->bruto_cas;
                    $total++;
                }
            }
            $dnevne_nalog->first()->totalNeto = $totalNeto;
            $dnevne_nalog->first()->totalBruto = $totalBruto;
            $dnevne_nalog->first()->totalItems = $total;

            return Dnevne_nalog_resource::collection($dnevne_nalog);
        } else
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    public function obsegDelavec(Request $request, $id)
    {
        if ($this->authAPI($request->key)) {
            $today = date("Y-m-d");
            $old = date("1-01-01");
            $totalCas = 0;
            if (!is_null($request->od) && is_null($request->do)) {
                $dnevne_nalog = Dnevne_nalog::join('dnevne_nalog_delavci', 'dnevne_nalog.id', '=', 'dnevne_nalog_delavci.dnevne_nalog_id')
                    ->join('delavci', 'delavci.id', '=', 'dnevne_nalog_delavci.delavci_id')
                    ->join('vozilo', 'vozilo.id', '=', 'dnevne_nalog.vozilo_id')
                    ->join('delovni_nalog', 'delovni_nalog.id', 'dnevne_nalog.delovni_nalog_id')
                    ->where('delavci.id', $id)->whereBetween('datum', [$request->od, $today])->select('dnevne_nalog.*', 'delovni_nalog.*', 'vozilo.naziv as vozilo', 'delavci.*')->paginate(5);
                $dnevne = Dnevne_nalog::join('dnevne_nalog_delavci', 'dnevne_nalog.id', '=', 'dnevne_nalog_delavci.dnevne_nalog_id')
                    ->join('delavci', 'delavci.id', '=', 'dnevne_nalog_delavci.delavci_id')
                    ->where('delavci.id', $id)->whereBetween('datum', [$request->od, $today])->get();
            } elseif (!is_null($request->od) && !is_null($request->do)) {
                $dnevne_nalog = Dnevne_nalog::join('dnevne_nalog_delavci', 'dnevne_nalog.id', '=', 'dnevne_nalog_delavci.dnevne_nalog_id')
                    ->join('delavci', 'delavci.id', '=', 'dnevne_nalog_delavci.delavci_id')
                    ->join('vozilo', 'vozilo.id', '=', 'dnevne_nalog.vozilo_id')
                    ->join('delovni_nalog', 'delovni_nalog.id', 'dnevne_nalog.delovni_nalog_id')
                    ->where('delavci.id', $id)->whereBetween('datum', [$request->od, $request->do])->select('dnevne_nalog.*', 'delovni_nalog.*', 'vozilo.naziv as vozilo', 'delavci.*')->paginate(5);
                $dnevne = Dnevne_nalog::join('dnevne_nalog_delavci', 'dnevne_nalog.id', '=', 'dnevne_nalog_delavci.dnevne_nalog_id')
                    ->join('delavci', 'delavci.id', '=', 'dnevne_nalog_delavci.delavci_id')
                    ->where('delavci.id', $id)->whereBetween('datum', [$request->od, $request->do])->get();
            } elseif (is_null($request->od) && !is_null($request->do)) {
                $dnevne_nalog = Dnevne_nalog::join('dnevne_nalog_delavci', 'dnevne_nalog.id', '=', 'dnevne_nalog_delavci.dnevne_nalog_id')
                    ->join('delavci', 'delavci.id', '=', 'dnevne_nalog_delavci.delavci_id')
                    ->join('vozilo', 'vozilo.id', '=', 'dnevne_nalog.vozilo_id')
                    ->join('delovni_nalog', 'delovni_nalog.id', 'dnevne_nalog.delovni_nalog_id')
                    ->where('delavci.id', $id)->whereBetween('datum', [$old, $request->do])->select('dnevne_nalog.*', 'delovni_nalog.*', 'vozilo.naziv as vozilo', 'delavci.*')->paginate(5);
                $dnevne = Dnevne_nalog::join('dnevne_nalog_delavci', 'dnevne_nalog.id', '=', 'dnevne_nalog_delavci.dnevne_nalog_id')
                    ->join('delavci', 'delavci.id', '=', 'dnevne_nalog_delavci.delavci_id')
                    ->where('delavci.id', $id)->whereBetween('datum', [$old, $request->do])->get();
            } else {
                $dnevne_nalog = Dnevne_nalog::join('dnevne_nalog_delavci', 'dnevne_nalog.id', '=', 'dnevne_nalog_delavci.dnevne_nalog_id')
                    ->join('delavci', 'delavci.id', '=', 'dnevne_nalog_delavci.delavci_id')
                    ->join('vozilo', 'vozilo.id', '=', 'dnevne_nalog.vozilo_id')
                    ->join('delovni_nalog', 'delovni_nalog.id', 'dnevne_nalog.delovni_nalog_id')
                    ->where('delavci.id', $id)->select('dnevne_nalog.datum', 'dnevne_nalog.startt', 'dnevne_nalog.zacetek_dela', 'dnevne_nalog.konec_dela', 'dnevne_nalog.prihod', 'delovni_nalog.sifra', 'dnevne_nalog.id', 'vozilo.naziv as vozilo', 'dnevne_nalog_delavci.je_sofer')->groupBy('dnevne_nalog.datum', 'dnevne_nalog.startt', 'dnevne_nalog.zacetek_dela', 'dnevne_nalog.konec_dela', 'dnevne_nalog.prihod', 'delovni_nalog.sifra', 'dnevne_nalog.id', 'vozilo.naziv', 'dnevne_nalog_delavci.je_sofer')->paginate(5);
                $dnevne = Dnevne_nalog::join('dnevne_nalog_delavci', 'dnevne_nalog.id', '=', 'dnevne_nalog_delavci.dnevne_nalog_id')
                    ->join('delavci', 'delavci.id', '=', 'dnevne_nalog_delavci.delavci_id')
                    ->where('delavci.id', $id)->get();
            }
            foreach ($dnevne as $dnevnaNaloga) {
                if ($dnevnaNaloga->je_sofer) {
                    $totalCas += Carbon::parse($dnevnaNaloga->prihod)->diffInSeconds(Carbon::parse($dnevnaNaloga->startt));
                } else {
                    $totalCas += Carbon::parse($dnevnaNaloga->konec_dela)->diffInSeconds(Carbon::parse($dnevnaNaloga->zacetek_dela));
                }
            }
            if (count($dnevne_nalog) > 0)
                $dnevne_nalog->first()->monterjev_cas = $totalCas / 3600;

            return Dnevne_nalog_resource::collection($dnevne_nalog);
        } else
            return ['method' => 'get', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    public function export(Request $request)
    {
        if ($this->authAPI($request->key)) {
            $today = date("Y-m-d");
            $old = date("1-01-01");
            $totalNeto = 0;
            $totalBruto = 0;
            $data = collect();
            if (!is_null($request->od) && is_null($request->do)) {
                $dnevne = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$request->od, $today])->get();
            } elseif (!is_null($request->od) && !is_null($request->do)) {
                $dnevne = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$request->od, $request->do])->get();
            } elseif (is_null($request->od) && !is_null($request->do)) {
                $dnevne = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$old, $request->do])->get();
            } else {
                $dnevne = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->get();
            }
            foreach ($dnevne as $key => $dnevnaNaloga) {
                $vozilo = $dnevnaNaloga->vozilo->first()->naziv;
                unset($dnevnaNaloga->vozilo);
                $dnevnaNaloga->vozilo = $vozilo;
                if (!is_null($request->vozilo) && strcasecmp($dnevnaNaloga->vozilo, $request->vozilo) != 0) {
                    unset($dnevne[$key]);
                }
                if (!is_null($request->objekt) && strcasecmp($dnevnaNaloga->delovni_nalog->first()->objekt, $request->objekt) != 0) {
                    unset($dnevne[$key]);
                }
                if (!is_null($request->sifra) && strcasecmp($dnevnaNaloga->delovni_nalog->first()->sifra, $request->sifra) != 0) {
                    unset($dnevne[$key]);
                }
                if (!is_null($request->delavec)) {
                    $flag = 0;
                    foreach ($dnevnaNaloga->dnevne_nalog_delavci as $delavec) {
                        $delavec->je_sofer = $delavec->pivot->je_sofer;
                        unset($delavec->pivot);
                        if (strcasecmp($delavec->ime, $request->delavec) == 0) $flag = 1;
                    }
                    if ($flag == 0) {
                        unset($dnevne[$key]);
                    }
                }
                if (isset($dnevne[$key])) {
                    $data[] = collect([
                        'sifra' => $dnevnaNaloga->delovni_nalog->first()->sifra,
                        'datum' => $dnevnaNaloga->datum,
                        'objekt' => $dnevnaNaloga->delovni_nalog->first()->objekt,
                        'vozilo' => $dnevnaNaloga->vozilo,
                        'odhod' => $dnevnaNaloga->startt,
                        'zacetek_dela' => $dnevnaNaloga->zacetek_dela,
                        'konec_dela' => $dnevnaNaloga->konec_dela,
                        'prihod' => $dnevnaNaloga->prihod,
                        'odsotnost_soferja' => $dnevnaNaloga->odsotnost_soferja,
                        'odsotnost_delavca' => $dnevnaNaloga->odsotnost_delavca,
                        'bruto_cas' => $dnevnaNaloga->bruto_cas,
                        'neto_cas' => $dnevnaNaloga->neto_cas
                    ]);
                    $totalNeto += $dnevnaNaloga->neto_cas;
                    $totalBruto += $dnevnaNaloga->bruto_cas;
                }
            }
            $data->push(['skupaj_neto' => "SKUPAJ NETO: ".$totalNeto, 'skupaj_brutp' => "SKUPAJ BRUTO: ".$totalBruto]);
            $export = new DnevneExport($data);
            return Excel::download($export, "export_" . date('d-m-Y') . ".xlsx");
        } else
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'Token expired'];
    }

    public function search(Request $request)
    {
        if ($request->key == "tempKey123!") {
            $today = date("Y-m-d");
            if (!is_null($request->od) && is_null($request->do)) {
                $dnevne_nalog = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$request->od, $today])->paginate(50);
            } elseif (!is_null($request->od) && !is_null($request->do)) {
                $dnevne_nalog = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->whereBetween('datum', [$request->od, $request->do])->paginate(50);
            } else {
                $dnevne_nalog = Dnevne_nalog::with('delovni_nalog', 'vozilo', 'dnevne_nalog_delavci')->paginate(50);
            }
            foreach ($dnevne_nalog as $key => $dnevnaNaloga) {
                $vozilo = $dnevnaNaloga->vozilo->first()->naziv;
                unset($dnevnaNaloga->vozilo);
                $dnevnaNaloga->vozilo = $vozilo;
                if (!is_null($request->vozilo) && strcasecmp($dnevnaNaloga->vozilo, $request->vozilo) != 0) {
                    unset($dnevne_nalog[$key]);
                }
                if (!is_null($request->objekt) && strcasecmp($dnevnaNaloga->delovni_nalog->first()->objekt, $request->objekt) != 0) {
                    unset($dnevne_nalog[$key]);
                }
                if (!is_null($request->sifra) && strcasecmp($dnevnaNaloga->delovni_nalog->first()->sifra, $request->sifra) != 0) {
                    unset($dnevne_nalog[$key]);
                }
                if (!is_null($request->delavec)) {
                    $flag = 0;
                    foreach($dnevnaNaloga->dnevne_nalog_delavci as $delavec) {
                        $delavec->je_sofer = $delavec->pivot->je_sofer;
                        unset($delavec->pivot);
                        if(strcasecmp($delavec->ime, $request->delavec) == 0) $flag = 1;
                    }
                    if($flag==0) {
                        unset($dnevne_nalog[$key]);
                    }
                }
            }
            return Dnevne_nalog_resource::collection($dnevne_nalog);
        } else
            return ['method' => 'post', 'response' => 'fail', 'reason' => 'key missing or incorrect'];
    }
}
