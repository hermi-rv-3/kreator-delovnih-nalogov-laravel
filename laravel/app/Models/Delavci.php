<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delavci extends Model
{
    use HasFactory;
    
    protected $table = "delavci";

    protected $fillable = ['ime', 'priimek', 'aktivan'];


    public function dnevne_nalog() {
        return $this->belongsToMany('App\Models\Dnevne_nalog', 'dnevne_nalog_delavci', 'delavci_id', 'dnevne_nalog_id')->withPivot('je_sofer')->with('delovni_nalog', 'vozilo');
    }
}
