<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dnevne_nalog_delavci extends Model
{
    use HasFactory;

    protected $table = "dnevne_nalog_delavci";

    protected $fillable = ['dnevne_nalog_id', 'delavci_id', 'je_sofer'];
}
