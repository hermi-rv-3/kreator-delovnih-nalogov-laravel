<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Dnevne_nalog extends Model
{
    use HasFactory;

    protected $table = "dnevne_nalog";

    protected $fillable = ['delovni_nalog_id', 'vozilo_id', 'datum', 'startt', 'zacetek_dela', 'konec_dela', 'prihod', 'odsotnost_soferja', 'odsotnost_delavca', 'neto_cas', 'bruto_cas'];
    
    public function delovni_nalog() {
        return $this->hasMany('App\Models\Delovni_nalog', 'id', 'delovni_nalog_id');
    }

    public function vozilo() {
        return $this->hasMany('App\Models\Vozilo', 'id', 'vozilo_id');
    }

    public function dnevne_nalog_delavci() {
        return $this->belongsToMany('App\Models\Delavci', 'dnevne_nalog_delavci', 'dnevne_nalog_id', 'delavci_id')->withPivot('je_sofer');
    }

}
