<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vozilo extends Model
{
    use HasFactory;

    protected $table = "vozilo";

    protected $fillable = ['naziv', 'aktivan'];
}
