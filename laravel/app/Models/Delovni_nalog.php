<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delovni_nalog extends Model
{
    use HasFactory;

    protected $table = "delovni_nalog";

    protected $fillable = ['datum_zacetka', 'sifra', 'naziv', 'objekt', 'status_projekt', 'neto_cas', 'bruto_cas'];

    public function delovni_nalog() {
        return $this->hasMany('App\Models\Dnevne_nalog', 'delovni_nalog_id');
    }

    public function dnevne_nalog() {
        return $this->delovni_nalog()->with('dnevne_nalog_delavci', 'vozilo');
    }

}
