<?php

namespace App\Exports;

use App\Models\Dnevne_nalog;
use Maatwebsite\Excel\Concerns\FromCollection;

class DnevneExport implements FromCollection
{
    public function collection()
    {
        return Dnevne_nalog::all();
    }
}