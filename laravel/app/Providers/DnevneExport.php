<?php

namespace App\Providers;

use App\Models\Dnevne_nalog;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DnevneExport implements FromCollection, WithHeadings
{
    public $dnevne;

    function __construct($dnevne) {
        $this->dnevne = $dnevne;
    }

    public function collection()
    {
        return $this->dnevne;
    }
    
    public function headings(): array
    {
        return [
            'Šifra',
            'Datum',
            'Objekt',
            'Vozilo',
            'Odhod',
            'Začetek dela',
            'Konec dela',
            'Prihod',
            'Odsotnost šoferja',
            'Odsotnost delavca',
            'Bruto čas',
            'Neto čas',
        ];
    }
}