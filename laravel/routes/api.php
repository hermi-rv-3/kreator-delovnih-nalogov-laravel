<?php

use App\Http\Controllers\Delovni_nalog_controller;
use App\Http\Controllers\Dnevne_nalog_controller;
use App\Http\Controllers\Delavci_controller;
use App\Http\Controllers\Vozilo_controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//DELOVNI NALOG
Route::get('/delovni-nalog', [Delovni_nalog_controller::class,'index']);
Route::get('/delovni-nalog/{id}', [Delovni_nalog_controller::class,'show']);
<<<<<<< HEAD

=======
>>>>>>> dnevne-naloge
Route::post('/delovni-nalog', [Delovni_nalog_controller::class,'store']);
Route::put('/delovni-nalog/{id}', [Delovni_nalog_controller::class,'update']);

//DNEVNE NALOG
Route::get('/dnevne-nalog', [Dnevne_nalog_controller::class,'index']);
Route::get('/dnevne-nalog/{id}', [Dnevne_nalog_controller::class,'show']);
<<<<<<< HEAD
Route::post('/dnevne-nalog/obseg', [Dnevne_nalog_controller::class,'obseg']);
Route::post('/dnevne-nalog/search', [Dnevne_nalog_controller::class,'search']);

=======
Route::get('/dnevne-nalog/search/export', [Dnevne_nalog_controller::class,'export']);
Route::post('/dnevne-nalog/search', [Dnevne_nalog_controller::class,'search']);
Route::post('/delavci/obseg/{id}', [Dnevne_nalog_controller::class,'obsegDelavec']);
>>>>>>> dnevne-naloge
Route::post('/dnevne-nalog', [Dnevne_nalog_controller::class,'store']);
Route::put('/dnevne-nalog/{id}', [Dnevne_nalog_controller::class,'update']);


//DELAVCI
Route::get('/delavci', [Delavci_controller::class,'index']);
Route::get('/delavci/{id}', [Delavci_controller::class,'show']);
<<<<<<< HEAD
Route::get('/delavci/dnevne-nalog/{id}', [Delavci_controller::class,'showDnevneNalog']);
Route::post('/delavci/obseg/{id}', [Delavci_controller::class,'obseg']);

=======
>>>>>>> dnevne-naloge
Route::post('/delavci', [Delavci_controller::class,'store']);
Route::put('/delavci/{id}', [Delavci_controller::class,'update']);

//VOZILA
Route::get('/vozilo', [Vozilo_controller::class,'index']);
Route::get('/vozilo/{id}', [Vozilo_controller::class,'show']);
<<<<<<< HEAD

=======
>>>>>>> dnevne-naloge
Route::post('/vozilo', [Vozilo_controller::class,'store']);
Route::put('/vozilo/{id}', [Vozilo_controller::class,'update']);