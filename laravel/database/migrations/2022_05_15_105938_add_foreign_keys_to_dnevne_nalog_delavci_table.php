<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDnevneNalogDelavciTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dnevne_nalog_delavci', function (Blueprint $table) {
            $table->foreign(['delavci_id'], 'c_delavci_id')->references(['id'])->on('delavci')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign(['dnevne_nalog_id'], 'c_dnevne_nalog_id')->references(['id'])->on('dnevne_nalog')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dnevne_nalog_delavci', function (Blueprint $table) {
            $table->dropForeign('c_delavci_id');
            $table->dropForeign('c_dnevne_nalog_id');
        });
    }
}
