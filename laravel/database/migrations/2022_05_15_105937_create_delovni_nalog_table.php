<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDelovniNalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delovni_nalog', function (Blueprint $table) {
            $table->integer('id', true);
            $table->date('datum_zacetka');
            $table->string('sifra', 50);
            $table->string('naziv', 50);
            $table->string('objekt', 50);
            $table->boolean('status_projekt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delovni_nalog');
    }
}
