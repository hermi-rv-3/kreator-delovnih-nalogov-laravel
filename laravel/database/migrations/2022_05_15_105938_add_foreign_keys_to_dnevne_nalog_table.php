<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDnevneNalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dnevne_nalog', function (Blueprint $table) {
            $table->foreign(['vozilo_id'], 'c_vozilo_id')->references(['id'])->on('vozilo')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign(['delovni_nalog_id'], 'delovne_nalog_id')->references(['id'])->on('delovni_nalog')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dnevne_nalog', function (Blueprint $table) {
            $table->dropForeign('c_vozilo_id');
            $table->dropForeign('delovne_nalog_id');
        });
    }
}
