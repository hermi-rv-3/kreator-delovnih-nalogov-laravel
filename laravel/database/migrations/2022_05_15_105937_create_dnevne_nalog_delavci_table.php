<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDnevneNalogDelavciTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dnevne_nalog_delavci', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('dnevne_nalog_id')->index('c_dnevne_nalog_id');
            $table->integer('delavci_id')->index('c_delavci_id');
            $table->boolean('je_sofer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dnevne_nalog_delavci');
    }
}
