<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDnevneNalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dnevne_nalog', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('delovni_nalog_id')->index('delovne_nalog_id');
            $table->integer('vozilo_id')->index('c_vozilo_id');
            $table->date('datum');
            $table->time('startt');
            $table->time('zacetek_dela');
            $table->time('konec_dela');
            $table->time('prihod');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dnevne_nalog');
    }
}
