# Kreator delovnih nalog 
## Opis projekta
### Člani
Na tem projektu delamo 4 študenta ki študirajo na FERI-ju. Mi smo Miroslav Stankov, Vlado Solakov, Radovan Radic in Emilija Mitrovic. Vodja ekipe je pa Radovan Radic. Na backend delajo Miroslav in Vlado, na frontend pa delajo Emilija in Radovan.

### Opis 
Delamo za firmo pod nazivom Hermi, naš projekt je pa izdelat stran za delovnih in dnevnih nalog ki jih ima firma.

### Naloga
Naloga je izdelat popolnoma funkcionalno stran za izpis delovnih nalog, dnevno spremljanje (vnos datumov montaže s časom odhoda, časom pričetka in konca dela ter časom prihoda) poteka montaže ter na koncu pregled porabljenih ur po posameznem projektu (delovnem nalogu), po posameznem monterju, ter tudi v določenem časovnem obdobju ter tudi konec/zaključek projekta (delovnega naloga)

### Navodila
## Cilji
Ideja je razviti namizno aplikacijo, ki bi delovala na strežniku, ter bi omogočala izpis delovnega naloga, dnevno spremljanje (vnos datumov montaže s časom odhoda, časom pričetka in konca dela ter časom prihoda) poteka montaže ter na koncu pregled porabljenih ur po posameznem projektu (delovnem nalogu), po posameznem monterju, ter tudi v določenem časovnem obdobju ter tudi konec/zaključek projekta (delovnega naloga)

## Specifikacije
### Tehnologije
- Laravel
- MySQL
- GitLab

### Razvojno okolje
- Visual studio code

### Sledenje poteku dela
- GitLab

## Način dela
Skupaj kot tim se pogovarjamo kaj je potrebno narediti
Delo si razdelimo tako da ima vsaka oseba približno enako dela
Napredek je pribelezen na Gitlabu in tamo spremljamo napredek projekta
Tekom projekta če obstajajo težavo jih skupaj rešujemo

## Zagon aplikacije 
Nas frontend in backend so live na serveru in lahko dostopajo samo uporabniki ki smo jih mi registrirali. 
https://hermirv3.web.app

## Zagon aplikacije (Lokalno)
Za lokalni zagon aplikacije najprej moramo prenesti projekt od github (https://gitlab.com/hermi-rv-3/kreator-delovnih-nalogov-laravel.git) na nas racunalnik, potem si prenesimo in instaliramo composer (https://getcomposer.org/download/), prenesemo tud bazo in posodobimo .env datoteko z ustreznimi podatkimi za bazo. Potem v terminalu damo composer install in za zagon backenda v terminalu napisemo php artisan serve.
